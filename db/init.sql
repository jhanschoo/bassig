CREATE EXTENSION "pgcrypto"; -- note: must be superuser or using PostgreSQL >= 13

CREATE TYPE "zone" AS ENUM (
  'france',
  'iraq',
  'canada',
  'india',
  'russia'
);

CREATE TYPE "class" AS ENUM (
  'fixed income',
  'cash',
  'equity',
  'other'
);

CREATE TABLE "product" (
  "uuid" uuid DEFAULT gen_random_uuid() PRIMARY KEY,
  "code" char(4) NOT NULL UNIQUE,
  "name" text NOT NULL,
  "price" integer NOT NULL -- depending on req., might choose numeric instead
);

CREATE TABLE "product_zone" (
  "uuid" uuid DEFAULT gen_random_uuid() PRIMARY KEY,
  "product_uuid" uuid NOT NULL REFERENCES "product",
  "allocation_date" date NOT NULL, -- we omit checking that date is not in future
  "allocation_percentage" smallint NOT NULL CHECK ("allocation_percentage" > 0 AND "allocation_percentage" <= 100),
  "name" "zone" NOT NULL,
  UNIQUE ("product_uuid", "name")
);

CREATE TABLE "product_class" (
  "uuid" uuid DEFAULT gen_random_uuid() PRIMARY KEY,
  "product_uuid" uuid NOT NULL REFERENCES "product",
  "allocation_date" date NOT NULL, -- we omit checking that date is not in future
  "allocation_percentage" smallint NOT NULL CHECK ("allocation_percentage" > 0 AND "allocation_percentage" <= 100),
  "name" "class" NOT NULL,
  UNIQUE ("product_uuid", "name")
);

CREATE FUNCTION product_has_zone_class()
RETURNS TRIGGER AS
$$
BEGIN
  IF (
    SELECT count(*)
    FROM "product_zone" pz
    WHERE NEW."uuid" = pz."product_uuid"
  ) = 0 OR (
    SELECT count(*)
    FROM "product_class" pc
    WHERE NEW."uuid" = pc."product_uuid"
  ) = 0 THEN
    RAISE EXCEPTION 'product % has no corresponding zone or no corresponding class', NEW."uuid";
  END IF;
  RETURN NULL;
END;
$$
language plpgsql;

CREATE CONSTRAINT TRIGGER check_product_has_zone_class AFTER INSERT OR UPDATE
ON "product"
INITIALLY DEFERRED
FOR EACH ROW
EXECUTE FUNCTION product_has_zone_class();

CREATE FUNCTION product_zone_sums_to_100(pid uuid)
RETURNS boolean AS
$$
BEGIN
  RETURN (
    SELECT sum(pz."allocation_percentage")
    FROM "product_zone" pz
    WHERE pz."product_uuid" = pid
  ) = 100;
END;
$$
language plpgsql;

CREATE FUNCTION product_zone_sums_to_100_new()
RETURNS TRIGGER AS
$$
BEGIN
  IF product_zone_sums_to_100(NEW."product_uuid") THEN
    RETURN NULL;
  END IF;
  RAISE EXCEPTION 'product_zone of % does not sum to 100', NEW."product_uuid";
END;
$$
language plpgsql;

CREATE FUNCTION product_zone_sums_to_100_old()
RETURNS TRIGGER AS
$$
BEGIN
  IF product_zone_sums_to_100(OLD."product_uuid") THEN
    RETURN NULL;
  END IF;
  RAISE EXCEPTION 'product_zone of % does not sum to 100', OLD."product_uuid";
END;
$$
language plpgsql;

CREATE CONSTRAINT TRIGGER check_product_zone_sums_to_100_new AFTER INSERT OR UPDATE ON "product_zone"
INITIALLY DEFERRED
FOR EACH ROW
EXECUTE FUNCTION product_zone_sums_to_100_new();

CREATE CONSTRAINT TRIGGER check_product_zone_sums_to_100_old AFTER UPDATE OR DELETE ON "product_zone"
INITIALLY DEFERRED
FOR EACH ROW
EXECUTE FUNCTION product_zone_sums_to_100_old();

CREATE FUNCTION product_class_sums_to_100(pid uuid)
RETURNS boolean AS
$$
BEGIN
  RETURN (
    SELECT sum(pc."allocation_percentage")
    FROM "product_class" pc
    WHERE pc."product_uuid" = pid
  ) = 100;
END;
$$
language plpgsql;

CREATE FUNCTION product_class_sums_to_100_new()
RETURNS TRIGGER AS
$$
BEGIN
  IF product_class_sums_to_100(NEW."product_uuid") THEN
    RETURN NULL;
  END IF;
  RAISE EXCEPTION 'product_class of % does not sum to 100', NEW."product_uuid";
END;
$$
language plpgsql;

CREATE FUNCTION product_class_sums_to_100_old()
RETURNS TRIGGER AS
$$
BEGIN
  IF product_class_sums_to_100(OLD."product_uuid") THEN
    RETURN NULL;
  END IF;
  RAISE EXCEPTION 'product_class of % does not sum to 100', OLD."product_uuid";
END;
$$
language plpgsql;

CREATE CONSTRAINT TRIGGER check_product_class_sums_to_100_new AFTER INSERT OR UPDATE ON "product_class"
INITIALLY DEFERRED
FOR EACH ROW
EXECUTE FUNCTION product_class_sums_to_100_new();

CREATE CONSTRAINT TRIGGER check_product_class_sums_to_100_old AFTER UPDATE OR DELETE ON "product_class"
INITIALLY DEFERRED
FOR EACH ROW
EXECUTE FUNCTION product_class_sums_to_100_old();