import { Client } from "pg";
import tough from "tough-cookie";
import fetch, { Response } from "node-fetch";
import fetchCookieLib from "fetch-cookie";

const getNewClient = () => new Client({
  connectionTimeoutMillis: 5_000
});

let client = getNewClient();
let allDone = false;
const apiHost = process.env.APIHOST || "api";
const apiPort = process.env.APIPORT ? parseInt(process.env.APIPORT) : 3000;
const useHttps = process.env.APIHTTPS === "true";
const apiUrl = `http${useHttps ? "s"  : ""}://${apiHost}:${apiPort}`;

// NOTE: remember to test illegal parameters!

beforeAll(async () => {
  const waitForDb = new Promise<void>((res, rej) => {
    const waitForDbAux = async () => {
      try {
        console.log("connecting to db");
        await client.connect();
        console.log("connected to db");
        res();
      } catch (e) {
        if (allDone) {
          rej("unable to connect to db even after jest started exiting");
        }
        else {
          console.log(e);
          console.log("unable to connected to db... retrying");
          try {
            await client.end();
          } finally {
            client = getNewClient();
          }
          setTimeout(waitForDbAux, 5_000);
        }
      }
    };
    waitForDbAux();
  });
  const waitForApi = new Promise<void>((res, rej) => {
    const waitForApiAux = async () => {
      try {
        console.log("connecting to api");
        const fetchRes = await fetch(`${apiUrl}/.ready`);
        if (fetchRes.ok) {
          console.log("connected to api");
          res();
        } else {
          // api not ready
          throw new Error("api is not ready.");
        }
      } catch (e) {
        if (allDone) {
          rej("unable to connect to api even after jest started exiting");
        } else {
          console.log(e);
          console.log("unable to connected to api... retrying");
          setTimeout(waitForApiAux, 5_000);
        }
      }
    };
    waitForApiAux();
  });
  await Promise.all([waitForDb, waitForApi]);
}, 240_000);

afterAll(async () => {
  allDone = true;
  await client.end();
}, 10_000);

describe("db connection", () => {
  test("it should be able to support a query", async () => {
    expect.assertions(1);
    const res = await client.query('SELECT 7 AS "n"');
    expect(res.rows[0].n).toBe(7);
  });
});

describe("mock data", () => {
  test("it should be present", async () => {
    expect.assertions(1);
    expect((await client.query(`SELECT count(*) FROM "product"`)).rows[0].count).toBe("35152");
  });
});

describe("wrong routes and routes with wrong methods", () => {
  test("POST api/.ready should respond with 404", async () => {
    expect.assertions(1);
    const res = await fetch(`${apiUrl}/.ready`, { method: "POST" });
    expect(res.status).toBe(404);
  });
  test("GET api/login should respond with 404", async () => {
    expect.assertions(1);
    const res = await fetch(`${apiUrl}/login`);
    expect(res.status).toBe(404);
  });
  test("POST api/products should respond with 404", async () => {
    expect.assertions(1);
    const res = await fetch(`${apiUrl}/products`, { method: "POST" });
    expect(res.status).toBe(404);
  });
  test("GET api/foo should respond with 404", async () => {
    expect.assertions(1);
    const res = await fetch(`${apiUrl}/foo`);
    expect(res.status).toBe(404);
  });
});

describe("POST api/login", () => {
  test("it should successfully set a cookie", async () => {
    expect.assertions(2);
    const res = await fetch(`${apiUrl}/login`, { method: "POST" });
    expect(res.status).toBe(200);
    expect(res.headers.raw()["set-cookie"]?.find((str) =>
      str.startsWith("authenticated") && str.endsWith("Path=/")
    )).toBeDefined();
  });
});

describe("GET api/products", () => {
  test("it should respond to unauthenticated users with 503", async () => {
    expect.assertions(1);
    const res = await fetch(`${apiUrl}/products`);
    expect(res.status).toBe(401);
  });

  describe("when authenticated", () => {
    let toughCookie = new tough.CookieJar();
    let fetchCookie = fetchCookieLib(fetch) as typeof fetch;
    const productsUrlString = `${apiUrl}/products`;

    beforeEach(async () => {
      toughCookie = new tough.CookieJar();
      fetchCookie = fetchCookieLib(fetch, toughCookie) as typeof fetch;
      await fetchCookie(`${apiUrl}/login`, { method: "POST" });
    });

    describe("pagination", () => {
      test("it should return an empty array when requesting no items", async () => {
        expect.assertions(1);
        const url = new URL(productsUrlString);
        url.searchParams.append("itemsPerPage", "0");
        const res = await fetchCookie(url.toString());
        await expect(res.json()).resolves.toStrictEqual([]);
      });

      test("it should return an array of ten items when requesting ten items", async () => {
        expect.assertions(1);
        const url = new URL(productsUrlString);
        url.searchParams.append("itemsPerPage", "10");
        const res = await fetchCookie(url.toString());
        await expect(res.json()).resolves.toHaveLength(10);
      });

      test("it should return an array of ten items when requesting ten items in another page", async () => {
        expect.assertions(1);
        const url = new URL(productsUrlString);
        url.searchParams.append("itemsPerPage", "10");
        url.searchParams.append("pageNumber", "2");
        const res = await fetchCookie(url.toString());
        await expect(res.json()).resolves.toHaveLength(10);
      });

      test("it should return the same results in separate requests", async () => {
        expect.assertions(1);
        const url = new URL(productsUrlString);
        url.searchParams.append("itemsPerPage", "10");
        const res1 = await fetchCookie(url.toString());
        const res2 = await fetchCookie(url.toString());
        expect(await res1.json()).toStrictEqual(await res2.json());
      });

      test("the first page is zero", async () => {
        expect.assertions(1);
        const url1 = new URL(productsUrlString);
        url1.searchParams.append("itemsPerPage", "10");
        const url2 = new URL(productsUrlString);
        url2.searchParams.append("itemsPerPage", "10");
        url2.searchParams.append("pageNumber", "0");
        const res1 = await (await fetchCookie(url1.toString())).json();
        const res2 = await (await fetchCookie(url2.toString())).json();
        expect(res1).toStrictEqual(res2);
      });

      test("it should return in increasing order in the code across separate pages", async () => {
        expect.assertions(2);
        const url0 = new URL(productsUrlString);
        url0.searchParams.append("itemsPerPage", "10");
        url0.searchParams.append("pageNumber", "0");
        const url1 = new URL(productsUrlString);
        url1.searchParams.append("itemsPerPage", "10");
        url1.searchParams.append("pageNumber", "2");
        const url2 = new URL(productsUrlString);
        url2.searchParams.append("itemsPerPage", "10");
        url2.searchParams.append("pageNumber", "3");
        const res0 = await (await fetchCookie(url0.toString())).json();
        const res1 = await (await fetchCookie(url1.toString())).json();
        const res2 = await (await fetchCookie(url2.toString())).json();
        expect(res0[res0.length - 1].code < res1[0].code).toBe(true);
        expect(res1[res1.length - 1].code < res2[0].code).toBe(true);
      });
    });

    describe("filters", () => {
      test("it should return an empty array when no element is present in filterByCodes", async () => {
        expect.assertions(1);
        const url = new URL(productsUrlString);
        url.searchParams.append("filterByCodes", JSON.stringify([]));
        const res = await fetchCookie(url.toString());
        await expect(res.json()).resolves.toStrictEqual([]);
      });
      test("it should return an empty array when no element is present in filterByNames", async () => {
        expect.assertions(1);
        const url = new URL(productsUrlString);
        url.searchParams.append("filterByNames", JSON.stringify([]));
        const res = await fetchCookie(url.toString());
        await expect(res.json()).resolves.toStrictEqual([]);
      });
      test("it should return an empty array when no element is present in showOnly", async () => {
        expect.assertions(1);
        const url = new URL(productsUrlString);
        url.searchParams.append("showOnly", JSON.stringify([]));
        const res = await fetchCookie(url.toString());
        await expect(res.json()).resolves.toStrictEqual([]);
      });

      test("it should return an array of elements matching the codes in filterByCodes", async () => {
        expect.assertions(5);
        const codes = ["beee", "bbbb", "bddd", "bccc", "vvvv"];
        const url = new URL(productsUrlString);
        url.searchParams.append("filterByCodes", JSON.stringify(codes));
        const res = await (await fetchCookie(url.toString())).json();
        expect(res).toHaveLength(4);
        for (const code of res.map((e) => e.code)) {
          expect(codes.indexOf(code)).not.toBe(-1);
        }
      });

      test("it should return an array of elements matching the names in filterByNames", async () => {
        expect.assertions(5);
        const names = ["Izp8GCp", "UADmffoFcA1rHp", "aaaaaa", "c6ToK2kIMbqBpFTd6kBsLYwgNc", "E0D14GnzneebVj3cYZk6CTP", "E0D14GnzneebVj3cYZk6CTP"];
        const url = new URL(productsUrlString);
        url.searchParams.append("filterByNames", JSON.stringify(names));
        const res = await (await fetchCookie(url.toString())).json();
        expect(res).toHaveLength(4);
        for (const name of res.map((e) => e.name)) {
          expect(names.indexOf(name)).not.toBe(-1);
        }
      });

      test("it should return an array of elements matching the uuids in showOnly", async () => {
        expect.assertions(5);
        const uuids = [
          "0778c723-4da3-41be-9914-b02bd353e55c",
          "11111111-1111-1111-1111-111111111111",
          "60362495-a9a5-47dd-a053-e9491a89e18d",
          "d9791e2a-299b-4d8e-87b6-49e1e0c762a6",
          "0778c723-4da3-41be-9914-b02bd353e55c",
          "99fb15f3-828c-43ee-8b0f-b91e3e9748e2"
        ];
        const url = new URL(productsUrlString);
        url.searchParams.append("showOnly", JSON.stringify(uuids));
        const res = await (await fetchCookie(url.toString())).json();
        expect(res).toHaveLength(4);
        for (const uuid of res.map((e) => e.uuid)) {
          expect(uuids.indexOf(uuid)).not.toBe(-1);
        }
      });

      test("it should return a conjunction of the three filters", async () => {
        expect.assertions(2);
        const codes = ["beee", "bddd", "bccc", "vvvv"]
        const names = ["UADmffoFcA1rHp", "aaaaaa", "c6ToK2kIMbqBpFTd6kBsLYwgNc", "E0D14GnzneebVj3cYZk6CTP", "E0D14GnzneebVj3cYZk6CTP"];
        const uuids = [
          "11111111-1111-1111-1111-111111111111",
          "60362495-a9a5-47dd-a053-e9491a89e18d",
          "d9791e2a-299b-4d8e-87b6-49e1e0c762a6",
          "99fb15f3-828c-43ee-8b0f-b91e3e9748e2"
        ];
        const url = new URL(productsUrlString);
        url.searchParams.append("filterByCodes", JSON.stringify(codes));
        url.searchParams.append("filterByNames", JSON.stringify(names));
        url.searchParams.append("showOnly", JSON.stringify(uuids));
        const res = await (await fetchCookie(url.toString())).json();
        expect(res).toHaveLength(1);
        expect(res[0]).toMatchObject({
          uuid: "99fb15f3-828c-43ee-8b0f-b91e3e9748e2",
          code: "beee",
          name: "c6ToK2kIMbqBpFTd6kBsLYwgNc"
        });
      });

      test("it should be compatible with pagination", async () => {
        expect.assertions(3);
        const codes = ["beee", "bddd", "bccc", "vvvv"]
        const names = ["Izp8GCp", "UADmffoFcA1rHp", "aaaaaa", "c6ToK2kIMbqBpFTd6kBsLYwgNc", "E0D14GnzneebVj3cYZk6CTP", "E0D14GnzneebVj3cYZk6CTP"];
        const uuids = [
          "0778c723-4da3-41be-9914-b02bd353e55c",
          "11111111-1111-1111-1111-111111111111",
          "60362495-a9a5-47dd-a053-e9491a89e18d",
          "d9791e2a-299b-4d8e-87b6-49e1e0c762a6",
          "0778c723-4da3-41be-9914-b02bd353e55c"
        ];
        const url1 = new URL(productsUrlString);
        url1.searchParams.append("filterByCodes", JSON.stringify(codes));
        url1.searchParams.append("filterByNames", JSON.stringify(names));
        url1.searchParams.append("showOnly", JSON.stringify(uuids));
        url1.searchParams.append("itemsPerPage", "1");
        url1.searchParams.append("pageNumber", "1");
        const res1 = await (await fetchCookie(url1.toString())).json();
        expect(res1).toHaveLength(1);
        expect(res1[0]).toMatchObject({
          uuid: "0778c723-4da3-41be-9914-b02bd353e55c",
          code: "bddd",
          name: "E0D14GnzneebVj3cYZk6CTP"
        });
        const url2 = new URL(productsUrlString);
        url2.searchParams.append("filterByCodes", JSON.stringify(codes));
        url2.searchParams.append("filterByNames", JSON.stringify(names));
        url2.searchParams.append("showOnly", JSON.stringify(uuids));
        url2.searchParams.append("itemsPerPage", "1");
        url2.searchParams.append("pageNumber", "2");
        const res2 = await (await fetchCookie(url2.toString())).json();
        expect(res2).toHaveLength(0);
      });
    });

    describe("breakdowns", () => {
      test("it should return a nonempty array when showAssetClassBreakdown is true", async () => {
        expect.assertions(9);
        const url = new URL(productsUrlString);
        url.searchParams.append("itemsPerPage", "4");
        url.searchParams.append("showAssetClassBreakdown", "true");
        const res = await (await fetchCookie(url.toString())).json();
        expect(res).toHaveLength(4);
        for (const product of await (await fetchCookie(url.toString())).json()) {
          expect(product.assetClassBreakdown.length).toBeGreaterThan(0);
          let sum = 0
          for (const item of product.assetClassBreakdown) {
            sum += parseInt(item.allocationPercentage);
          }
          expect(sum).toBe(100);
        }
      });
      test("it should return a nonempty array when showGeographicalBreakdown is true", async () => {
        expect.assertions(9);
        const url = new URL(productsUrlString);
        url.searchParams.append("itemsPerPage", "4");
        url.searchParams.append("showGeographicalBreakdown", "true");
        const res = await (await fetchCookie(url.toString())).json();
        expect(res).toHaveLength(4);
        for (const product of await (await fetchCookie(url.toString())).json()) {
          expect(product.geographicalBreakdown.length).toBeGreaterThan(0);
          let sum = 0
          for (const item of product.geographicalBreakdown) {
            sum += parseInt(item.allocationPercentage);
          }
          expect(sum).toBe(100);
        }
      });

      test("it should have the correct types of properties", async () => {
        expect.assertions(7);
        const url = new URL(productsUrlString);
        url.searchParams.append("itemsPerPage", "1");
        url.searchParams.append("showAssetClassBreakdown", "true");
        url.searchParams.append("showGeographicalBreakdown", "true");
        const res = await (await fetchCookie(url.toString())).json();
        expect(res).toHaveLength(1);
        const product = res[0];
        const { assetClassBreakdown, geographicalBreakdown } = product;
        const assetClass = assetClassBreakdown[0];
        const geographical = geographicalBreakdown[0];
        expect(typeof assetClass.allocationDate).toBe("string");
        expect(typeof assetClass.allocationPercentage).toBe("string");
        expect(["fixed income", "cash", "equity", "other"].indexOf(assetClass.name)).not.toBe(-1);
        expect(typeof geographical.allocationDate).toBe("string");
        expect(typeof geographical.allocationPercentage).toBe("string");
        expect(["france", "iraq", "canada", "india", "russia"].indexOf(geographical.name)).not.toBe(-1);
      })
    });

    describe("product", () => {
      test("it should have the correct types of properties", async () => {
        expect.assertions(6);
        const url = new URL(productsUrlString);
        url.searchParams.append("itemsPerPage", "1");
        url.searchParams.append("showAssetClassBreakdown", "true");
        url.searchParams.append("showGeographicalBreakdown", "true");
        const res = await (await fetchCookie(url.toString())).json();
        expect(res).toHaveLength(1);
        const product = res[0];
        expect(typeof product.uuid).toBe("string");
        expect(typeof product.code).toBe("string");
        expect(typeof product.name).toBe("string");
        expect(product.assetClassBreakdown).toBeDefined();
        expect(product.geographicalBreakdown).toBeDefined();
      })
    });
  });
});
