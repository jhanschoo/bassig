# `api-test`

`api-test` is a project that builds a container image that performs automated Jest tests against a containerized `api` image, intended to be used as part of a CI/CD pipeline.

For now, pregenerated mock data residing in `dump.pgdata` is used, and hardcoded test values based on this data are used in the tests.

## Configurable Environment Variables

* `PGUSER`, `PGHOST`, `PGPASSWORD`, `PGDATABASE`, `PGPORT` are used to configure the connection to the db service
* `APIHOST`, `APIPORT` are used to configure the connection to the api service

## Organization

```
- test/
    directory containing all the test scripts that will be run
  010-restore-dump.sh, dump.pgdata
    these files are mounted to the postgres container in docker-compose.yml, seeding the db container for the tests
  
  // containerization
  dockerbuild.sh
    Script called to build the container. Configure the container image tag here.
  docker-compose.yml
    Configures the environment in which the tests are run. (Not used in the container image itself)
```
