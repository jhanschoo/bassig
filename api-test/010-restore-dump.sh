# https://stackoverflow.com/questions/29600369/starting-and-populating-a-postgres-container-in-docker/54946350#54946350
pg_restore -U postgres --dbname=postgres --verbose --single-transaction < /docker-entrypoint-initdb.d/dump.pgdata || exit 1
