#!/usr/bin/env sh

# Run in bassig/db-test

set -ex

PACKAGE_VERSION=$(node -pe "require('./package.json').version")
IMAGE_NAME="$CI_REGISTRY_IMAGE/db-test"

IMAGE_TAG="$IMAGE_NAME:$PACKAGE_VERSION"
CACHE_IMAGE_TAG="$IMAGE_NAME:latest-$CI_COMMIT_REF_SLUG"

docker pull "$CACHE_IMAGE_TAG" || true
docker build \
  --cache-from "$CACHE_IMAGE_TAG" \
  --tag "$IMAGE_TAG" \
  --tag "$CACHE_IMAGE_TAG" \
  .

docker push "$IMAGE_TAG"
docker push "$CACHE_IMAGE_TAG"
