import { Client } from "pg";
const getNewClient = () => new Client({
  connectionTimeoutMillis: 5_000
});

let client = getNewClient();
let isDone = false;

beforeAll(async () => {
  let connected = false;
  while (!connected && !isDone) {
    try {
      console.log("Attempting to connect...");
      await client.connect();
      connected = true;
    } catch (e) {
      console.log(e);
      console.log("Error connecting... retrying.");
      try {
        await client.end();
      } finally {
        client = getNewClient();
      }
    }
  }
  if (!connected) {
    throw new Error("unable to connect to db even after jest started exiting");
  }
}, 60_000);

beforeEach(async () => {
  await client.query('TRUNCATE TABLE "product", "product_zone", "product_class"');
})

afterAll(async () => {
  isDone = true;
  await client.end();
});

describe("connection", () => {
  test("it should be able to support a query", async () => {
    expect.assertions(1);
    const res = await client.query('SELECT 7 AS "n"');
    expect(res.rows[0].n).toBe(7);
  });
});

describe("schema triggers", () => {
  test("it should reject insertion of a bare product row", async () => {
    expect.assertions(1);
    await expect(client.query(`
    INSERT INTO "product" ("code", "name", "price") VALUES ('abcd', 'foo', 12345)
    `)).rejects.toThrow(/product .* has no corresponding zone or no corresponding class/);
  });

  test("it should accept a transaction inserting a product associated with a single zone and a single class", async () => {
    expect.assertions(1);
    try {
      await client.query('BEGIN');
      const productId = (await client.query(`
      INSERT INTO "product" ("code", "name", "price") VALUES ('abcd', 'foo', 12345) RETURNING "uuid"
      `)).rows[0].uuid;
      await client.query(`
      INSERT INTO "product_zone" ("product_uuid", "allocation_date", "allocation_percentage", "name")
      VALUES ($1, '2015-02-03'::date, 100, 'iraq')
      `, [productId]);
      await client.query(`
      INSERT INTO "product_class" ("product_uuid", "allocation_date", "allocation_percentage", "name")
      VALUES ($1, '2015-02-03'::date, 100, 'other')
      `, [productId]);
      await client.query('COMMIT');
      expect(true).toBe(true);
    } catch (e) {
      console.error(e);
      await client.query('ROLLBACK');
    }
  });

  test("it should reject a transaction inserting a product where the zones do not total 100 percent", async () => {
    expect.assertions(1);
    await expect(async () => {
      try {
        await client.query('BEGIN');
        const productId = (await client.query(`
        INSERT INTO "product" ("code", "name", "price") VALUES ('abcd', 'foo', 12345) RETURNING "uuid"
        `)).rows[0].uuid;
        await client.query(`
        INSERT INTO "product_zone" ("product_uuid", "allocation_date", "allocation_percentage", "name")
        VALUES ($1, '2015-02-03'::date, 90, 'iraq')
        `, [productId]);
        await client.query(`
        INSERT INTO "product_class" ("product_uuid", "allocation_date", "allocation_percentage", "name")
        VALUES ($1, '2015-02-03'::date, 100, 'other')
        `, [productId]);
        await client.query('COMMIT');
      } catch (e) {
        await client.query('ROLLBACK');
        throw e;
      }
    }).rejects.toThrow(/product_zone of .* does not sum to 100/);
  });

  test("it should reject a transaction inserting a product where the classes do not total 100 percent", async () => {
    expect.assertions(1);
    await expect(async () => {
      try {
        await client.query('BEGIN');
        const productId = (await client.query(`
        INSERT INTO "product" ("code", "name", "price") VALUES ('abcd', 'foo', 12345) RETURNING "uuid"
        `)).rows[0].uuid;
        await client.query(`
        INSERT INTO "product_zone" ("product_uuid", "allocation_date", "allocation_percentage", "name")
        VALUES ($1, '2015-02-03'::date, 100, 'iraq')
        `, [productId]);
        await client.query(`
        INSERT INTO "product_class" ("product_uuid", "allocation_date", "allocation_percentage", "name")
        VALUES ($1, '2015-02-03'::date, 90, 'other')
        `, [productId]);
        await client.query('COMMIT');
      } catch (e) {
        await client.query('ROLLBACK');
        throw e;
      }
    }).rejects.toThrow(/product_class of .* does not sum to 100/);
  });

  test("it should accept a transaction inserting a product associated with multiple zones and multiple classes", async () => {
    expect.assertions(1);
    try {
      await client.query('BEGIN');
      const productId = (await client.query(`
      INSERT INTO "product" ("code", "name", "price") VALUES ('abcd', 'foo', 12345) RETURNING "uuid"
      `)).rows[0].uuid;
      await client.query(`
      INSERT INTO "product_zone" ("product_uuid", "allocation_date", "allocation_percentage", "name")
      VALUES ($1, '2015-02-03'::date, 67, 'iraq'),
      ($1, '2015-03-04'::date, 33, 'india')
      `, [productId]);
      await client.query(`
      INSERT INTO "product_class" ("product_uuid", "allocation_date", "allocation_percentage", "name")
      VALUES ($1, '2015-02-03'::date, 50, 'other'),
      ($1, '2015-04-16'::date, 50, 'cash')
      `, [productId]);
      await client.query('COMMIT');
      expect(true).toBe(true);
    } catch (e) {
      console.error(e);
      await client.query('ROLLBACK');
    }
  });

});
