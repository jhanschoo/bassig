# `api-test`

`db-test` is a project that builds a container image that performs automated Jest tests against a `postgres` container, performing sanity checks on the schema it is initialized with.

## Configurable Environment Variables

* `PGUSER`, `PGHOST`, `PGPASSWORD`, `PGDATABASE`, `PGPORT` are used to configure the connection to the db service

## Organization

```
- test/
    directory containing all the test scripts that will be run
  ../db/init.sql
    initializes the schema for the database (this file is copied into this project folder in ../.gitlab-ci.yml)
  
  // containerization
  dockerbuild.sh
    Script called to build the container. Configure the container image tag here.
  docker-compose.yml
    Configures the environment in which the tests are run. (Not used in the container image itself)
```
