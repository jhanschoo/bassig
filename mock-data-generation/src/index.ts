import fs from "fs";
import Prando from "prando";
import { shuffleArray } from "./util";
import { insertProductString } from "./insertProductString";
import { insertZoneStrings } from "./insertZoneStrings";
import { insertClassStrings } from "./insertClassStrings";

const rng = new Prando(2143);

const alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
const alphabet2 = shuffleArray(alphabet, rng);
const alphabet3 = shuffleArray(alphabet, rng);
const alphabet4 = shuffleArray(alphabet, rng);

fs.open("data.sql", "w", (err, fd) => {
  if (err) {
    throw err;
  }
  try {
    fs.writeSync(fd, 'TRUNCATE TABLE "product", "product_zone", "product_class";\n');
    // random data
    for (const first of ["b", "c"]) {
      for (const second of alphabet2) {
        for (const third of alphabet3) {
          for (const fourth of alphabet4) {
            fs.writeSync(fd, "BEGIN;\n");
            const code = `${first}${second}${third}${fourth}`;
            fs.writeSync(fd, insertProductString(code, rng));
            fs.writeSync(fd, insertZoneStrings(code, rng));
            fs.writeSync(fd, insertClassStrings(code, rng));
            fs.writeSync(fd, "COMMIT;\n");
          }
        }
      }
    }
  } finally {
    fs.close(fd, (err) => {
      if (err) {
        throw err;
      }
    });
  }
});
