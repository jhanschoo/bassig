import Prando from "prando";
import { shuffleArray, nextDate } from "./util";

const classes = ["fixed income", "cash", "equity", "other"];

const insertClassString = (code: string, date: string, percentage: number, cl: string) => {
    return `INSERT INTO "product_class" ("product_uuid", "allocation_date", "allocation_percentage", "name")
    SELECT "product"."uuid", '${date}'::date, ${percentage}, '${cl}'
    FROM "product"
    WHERE "product"."code" = '${code}';\n`;
}

export const insertClassStrings = (code: string, rng: Prando): string => {
  const numClasses = rng.nextInt(1, 4);
  let remainder = 100;
  const shuffled_classes = shuffleArray(classes, rng).slice(0, numClasses);
  const statements = [];
  for (let i = 0; i < shuffled_classes.length - 1; i++) {
    const date = nextDate(rng);
    const percentage = rng.nextInt(1, remainder - numClasses + i + 1);
    remainder -= percentage;
    statements.push(insertClassString(code, date, percentage, shuffled_classes[i]));
  }
  const date = nextDate(rng);
  statements.push(insertClassString(code, date, remainder, shuffled_classes[shuffled_classes.length - 1]));
  return statements.join("");
};
