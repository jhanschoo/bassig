import Prando from "prando";

const randomName = (rng: Prando) => {
  const length = rng.nextInt(0, 40);
  return rng.nextString(length);
}

export const insertProductString = (code: string, rng: Prando): string => {
  const name = randomName(rng);
  const price = rng.nextInt(0, 90000);
  return `INSERT INTO "product" ("code", "name", "price") VALUES ('${code}', '${name}', ${price});\n`;
}
