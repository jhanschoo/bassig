import Prando from "prando";
import { shuffleArray, nextDate } from "./util";

const zones = ["france", "iraq", "canada", "india", "russia"];

const insertZoneString = (code: string, date: string, percentage: number, zone: string) => {
    return `INSERT INTO "product_zone" ("product_uuid", "allocation_date", "allocation_percentage", "name")
    SELECT "product"."uuid", '${date}'::date, ${percentage}, '${zone}'
    FROM "product"
    WHERE "product"."code" = '${code}';\n`;
}

export const insertZoneStrings = (code: string, rng: Prando): string => {
  const numZones = rng.nextInt(1, 5);
  let remainder = 100;
  const shuffled_zones = shuffleArray(zones, rng).slice(0, numZones);
  const statements = [];
  for (let i = 0; i < shuffled_zones.length - 1; i++) {
    const date = nextDate(rng);
    const percentage = rng.nextInt(1, remainder - numZones + i + 1);
    remainder -= percentage;
    statements.push(insertZoneString(code, date, percentage, shuffled_zones[i]));
  }
  const date = nextDate(rng);
  statements.push(insertZoneString(code, date, remainder, shuffled_zones[shuffled_zones.length - 1]));
  return statements.join("");
};
