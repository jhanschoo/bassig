import Prando from "prando";

// Shamelessly adapted from
// https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
export const shuffleArray = <T>(array: T[], rng: Prando): T[] => {
  const a = array.slice();
  for (let i = 0; i < a.length - 1; i++) {
    const j = rng.nextInt(i, a.length - 1);
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
};

export const nextDate = (rng: Prando): string => {
  const year = 1980 + rng.nextInt(18);
  const month = rng.nextInt(1, 12);
  const day = rng.nextInt(1, 27);
  return `${year}-${month}-${day}`;
};
