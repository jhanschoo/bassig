# `mock-data-generation`

This project generates mock data for populating the database, using a deterministic PRNG library.

```
npm i
npm run start
# a data.sql file containing INSERT statements will be generated.
```
