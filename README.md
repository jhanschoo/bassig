# bassig

* `api` contains the project that is built into an Express app serving the desired API.
* `api-test` builds into a container containing tests against `api`, for use in testing `api` in a CI/CD environment.
* `db` contains a single script `init.sql` that defines the PostgreSQL database schema backing `api`.
* `db-test` builds into a container containing tests against the database schema, for use in a CI/CD environment.
* `mock-data-generation` is a project that generates mock data to populate a test DB for use with `api-test`.