# `api`

`api` is an Express app that serves two endpoints `POST /login` and `GET /products`. Calling `POST /login` sets a cookie authenticating the user, and the cookie needs to be present for `GET /products` to be successfully accessed.

## Configurable Environment Variables

* `PGUSER`, `PGHOST`, `PGPASSWORD`, `PGDATABASE`, `PGPORT` are used to configure the connection to the db service
* `PORT` configures the port that the api serves on

HTTPS is not supported for now, and SSL is expected to be terminated elsewhere.

## Organization

The project is organized as a typical Express app.
```
- src/
  - models/
      TypeScript interfaces modeling the objects returned by the API
  - routes/ 
      Express routing handlers
  - services/
      Data sources querying the db
  app.ts
    Express app initialization
  db.ts
    Db service initialization (via pg-promise)
  index.ts
    Entry point
  types.ts
    Miscellaneous types
  util.ts
    General helper functions
  
  // containerization
  dockerbuild.sh
    Script called to build the container. Configure the container image tag here.
```
