import { ProductClass } from "./ProductClass";
import { ProductZone } from "./ProductZone";

export interface Product {
  uuid: string;
  code: string;
  name: string;
  assetClassBreakdown?: ProductClass[];
  geographicalBreakdown?: ProductZone[];
}
