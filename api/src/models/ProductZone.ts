export enum ZoneNames {
  france = "france",
  iraq = "iraq",
  canada = "canada",
  india = "india",
  russia = "russia",
}

export interface ProductZone {
  allocationDate: string;
  allocationPercentage: string;
  name: ZoneNames;
}
