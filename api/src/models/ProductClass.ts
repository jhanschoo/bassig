export enum ClassNames {
  fixedIncome = "fixed income",
  cash = "cash",
  equity = "equity",
  other = "other"
}

export interface ProductClass {
  allocationDate: string;
  allocationPercentage: string;
  name: ClassNames;
}
