export const parseIntegerQueryParam = (str: string | string[] | undefined): number | undefined => {
  if (str === undefined) {
    return undefined;
  }
  if (typeof str !== "string") {
    throw new Error(`parseIntegerQueryParam: argument ${str} is not a string`);
  }
  const unsafeNumber = parseInt(str);
  if (!isFinite(unsafeNumber)) {
    throw new Error(`parseIntegerQueryParam: argument ${str} is not integer-like`);
  }
  return unsafeNumber;
}

export const parseBooleanQueryParam = (str: string | string[] | undefined): boolean | undefined => {
  if (str === undefined) {
    return undefined;
  }
  if (str === "true") {
    return true;
  }
  if (str === "false") {
    return false;
  }
  throw new Error(`parseBooleanQueryParam: argument ${str} is neither the string "true" nor the string "false"`);
}

export const parseStringArrayQueryParam = (str: string | string[] | undefined): string[] | undefined => {
  if (str === undefined) {
    return undefined;
  }
  if (typeof str !== "string") {
    throw new Error(`parseIntegerQueryParam: argument ${str} is not a string`);
  }
  const res = JSON.parse(str);
  if (!Array.isArray(res)) {
    throw new Error(`parseIntegerQueryParam: argument ${str} is not a stringified JSON array`);
  }
  if (!res.every((e): e is string => typeof e === "string")) {
    throw new Error(`parseIntegerQueryParam: argument ${str} is a JSON array containing a non-string element`);
  }
  return res;
}

export const dateToDateString = (d: Date) => d.toISOString().slice(0, 10);