import pgpromise from "pg-promise";

const pgp = pgpromise();

export const db = pgp({
  user: process.env.PGUSER || process.env.USER,
  host: process.env.PGHOST || "localhost",
  password: process.env.PGPASSWORD || undefined,
  database: process.env.PGDATABASE || process.env.USER,
  port: process.env.PGPORT ? parseInt(process.env.PGPORT) : 5432,
  connectionTimeoutMillis: 5_000,
});

// db lazily initializes connection, so we perform a query here to quickly
//   fail if connection parameters are bad
let tries = 0;
const tryConnection = () => {
  db.one("SELECT 7").catch((err) => {
    if (tries < 10) {
      ++tries;
      setTimeout(tryConnection, 10_000);
    }
    else {
      process.exit(1);
    }
  });
}
tryConnection();
