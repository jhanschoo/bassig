import express from "express";
import helmet from "helmet";
import cookieParser from "cookie-parser";
import { registerRoutes } from "./routes";

export const app = express();
app.set("query parser", "simple");
app.use(helmet());
app.use(cookieParser(process.env.COOKIE_SECRET || Math.random().toString()));

registerRoutes(app);

const port = process.env.PORT ? parseInt(process.env.PORT) : 3000;
app.listen(port, () => {
  console.log(`API server listening at port ${port}.`)
});
