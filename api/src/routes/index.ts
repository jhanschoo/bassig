import type { Express } from "express";
import { authenticatedGate } from "./hoc/authenticatedGate";
import { readyHandler } from "./.ready";
import { loginHandler } from "./login";
import { productsHandler } from "./products";

export const registerRoutes = (app: Express): void => {
  app.get("/.ready", readyHandler);
  app.post("/login", loginHandler);
  app.get("/products", authenticatedGate(productsHandler));
};
