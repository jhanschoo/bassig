import type { RequestHandler } from "../types";
import { db } from "../db";

let dbReady = false;
let backoff = 1_000;
const tryConnection = async () => {
  try {
    await db.one("SELECT 7");
    dbReady = true;
  } catch (e) {
      setTimeout(tryConnection, backoff);
  }
}
tryConnection();

export const readyHandler: RequestHandler = (req, res) => {
  if (dbReady) {
    res.end();
  } else {
    res.status(503).end();
  }
};
