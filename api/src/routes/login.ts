import type { RequestHandler } from "../types";

export const loginHandler: RequestHandler = (req, res) => {
  res.cookie("authenticated", "true", { signed: true });
  res.end();
};
