import { RequestHandler } from "../../types";

export const authenticatedGate = (handler: RequestHandler): RequestHandler =>
  (req, res, ...args) => {
    if (req.signedCookies["authenticated"] != "true") {
      res.status(401).end();
    } else {
      return handler(req, res, ...args);
    }
  };
