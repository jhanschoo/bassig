import type { RequestHandler } from "../types";
import type { ParsedUrlQuery } from "querystring";

import { productService } from "../services/productService";

import {
  parseIntegerQueryParam,
  parseBooleanQueryParam,
  parseStringArrayQueryParam,
} from "../util";

const getProductsParams = (reqQuery: ParsedUrlQuery) => ({
  itemsPerPage: parseIntegerQueryParam(reqQuery.itemsPerPage),
  pageNumber: parseIntegerQueryParam(reqQuery.pageNumber),
  showAssetClassBreakdown: parseBooleanQueryParam(reqQuery.showAssetClassBreakdown),
  showGeographicalBreakdown: parseBooleanQueryParam(reqQuery.showGeographicalBreakdown),
  filterByCodes: parseStringArrayQueryParam(reqQuery.filterByCodes),
  filterByNames: parseStringArrayQueryParam(reqQuery.filterByNames),
  showOnly: parseStringArrayQueryParam(reqQuery.showOnly),
});

export const productsHandler: RequestHandler = async (req, res) => {
  let params: ReturnType<typeof getProductsParams>;
  try {
    params = getProductsParams(req.query);
    if (params.pageNumber !== undefined && params.itemsPerPage === undefined) {
      throw new Error("productsHandler: pageNumber is specified but itemsPerPage is not specified");
    }
  } catch (e) {
    console.error(e);
    res.status(400).send(e.toString())
    return;
  }
  try {
    const products = await productService.getMany({
      take: params.itemsPerPage,
      skip: params.pageNumber !== undefined
        ? params.pageNumber * (params.itemsPerPage as number)
        : undefined,
      codeFilter: params.filterByCodes,
      nameFilter: params.filterByNames,
      uuidFilter: params.showOnly,
      includeClass: params.showAssetClassBreakdown,
      includeZone: params.showGeographicalBreakdown
    });
    res.json(products);
  } catch (e) {
    console.error(e);
    res.status(500).end();
    return;
  }
};
