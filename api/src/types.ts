import type { RequestHandler } from "express";
import type { ParsedUrlQuery } from "querystring";

type RH = RequestHandler<any, any, any, ParsedUrlQuery>;

export { RH as RequestHandler };