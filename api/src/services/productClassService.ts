import { db } from "../db";
import { ProductClass } from "../models/ProductClass";
import { dateToDateString } from "../util";

export interface ProductClassService {
  getByProduct(productUUID: string, options: {}): Promise<ProductClass[]>;
}

export const productClassService: ProductClassService = {
  async getByProduct(productUUID, _options) {
    let query = `
    SELECT "allocation_date", "allocation_percentage", "name"
    FROM "product_class"
    WHERE "product_uuid" = $<productUUID>
    ORDER BY "name"`;

    const rows: ProductClass[] = (await db.any(query, {
      productUUID,
    })).map((row) => ({
      allocationDate: dateToDateString(row["allocation_date"]),
      allocationPercentage: row["allocation_percentage"].toString(),
      name: row.name
    }));
    return rows;
  }
}
