import { db } from "../db";
import { Product } from "../models/Product";
import { productClassService } from "./productClassService";
import { productZoneService } from "./productZoneService";

export interface ProductServiceFilterOptions {
  codeFilter?: string[];
  nameFilter?: string[];
  uuidFilter?: string[];
}

// Note: ProductClass and ProductZone records are weak entities wrt Product
// i.e. they have no meaningful
// existence outside of the Product record they are associated
// with, and only that Product record, hence it should not cause
// problems to allow for their retrieval as part of retrieval of
// Product records.
export interface ProductServiceDataOptions {
  includeClass?: boolean;
  includeZone?: boolean;
}

export interface ProductServicePaginationOptions {
  take?: number;
  skip?: number;
}

export type ProductServiceGetManyOptions =
  & ProductServiceFilterOptions
  & ProductServiceDataOptions
  & ProductServicePaginationOptions
  ;

export interface ProductService {
  getMany(options: ProductServiceGetManyOptions): Promise<Product[]>;
}

export const productService: ProductService = {
  async getMany(params) {
    const {
      codeFilter, nameFilter, uuidFilter, includeClass, includeZone, take, skip
    } = params;
    let query = `
    SELECT "uuid", "code", "name", "price"
    FROM "product"
    WHERE TRUE`;

    // filters
    if (codeFilter !== undefined) {
      query += `
      AND "code" = ANY ($<codeFilter>::text[])`;
    }
    if (nameFilter !== undefined) {
      query += `
      AND "name" = ANY ($<nameFilter>::text[])`;
    }
    if (uuidFilter !== undefined) {
      query += `
      AND "uuid" = ANY ($<uuidFilter>::uuid[])`;
    }

    query += `
    ORDER BY "code"`;

    // pagination
    if (take !== undefined) {
      query += `
      LIMIT $<take>`;
    }
    if (skip !== undefined) {
      query += `
      OFFSET $<skip>`;
    }

    const rows: Product[] = await db.any(query, params);

    const dataPromises: Promise<void[]>[] = [];
    if (includeClass) {
      dataPromises.push(Promise.all(rows.map(async (product) => {
        product.assetClassBreakdown = await productClassService.getByProduct(product.uuid, {});
      })));
    }
    if (includeZone) {
      dataPromises.push(Promise.all(rows.map(async (product) => {
        product.geographicalBreakdown = await productZoneService.getByProduct(product.uuid, {});
      })));
    }
    await Promise.all(dataPromises);
    return rows;
  }
};