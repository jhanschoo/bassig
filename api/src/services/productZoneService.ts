import { db } from "../db";
import { ProductZone } from "../models/ProductZone";
import { dateToDateString } from "../util";

export interface ProductZoneService {
  getByProduct(productUUID: string, options: {}): Promise<ProductZone[]>;
}

export const productZoneService: ProductZoneService = {
  async getByProduct(productUUID, _options) {
    let query = `
    SELECT "allocation_date", "allocation_percentage", "name"
    FROM "product_zone"
    WHERE "product_uuid" = $<productUUID>
    ORDER BY "name"`;

    const rows: ProductZone[] = (await db.any(query, {
      productUUID,
    })).map((row) => ({
      allocationDate: dateToDateString(row["allocation_date"]),
      allocationPercentage: row["allocation_percentage"].toString(),
      name: row.name
    }));
    return rows;
  }
}
